const mongoose = require('mongoose');
const User = require('./models/user');
const Event = require('./models/event');
const Group = require('./models/group');
const Thread = require('./models/thread');
const Message = require('./models/message');
const seedData = require('./data/randomData.json');
const dotenv = require('dotenv');

// Load environment variables
dotenv.config();

const mongoUri = process.env.MONGODB_URI;

async function seedDatabase() {
  try {
    await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('Connected to MongoDB for seeding.');

    // Clear existing data
    await User.deleteMany({});
    await Event.deleteMany({});
    await Group.deleteMany({});
    await Thread.deleteMany({});
    await Message.deleteMany({});

    // Insert users
    const userPromises = seedData.users.map(user => new User(user).save());
    const users = await Promise.all(userPromises);

    // Map email to user ID
    const emailToUserId = {};
    users.forEach(user => {
      emailToUserId[user.email] = user._id;
    });


    // Insert events
    const eventPromises = seedData.events.map(event => {
      const organizers = event.organizers.map(email => emailToUserId[email]);
      const members = event.members.map(email => emailToUserId[email]);
      return new Event({
        ...event,
        organizers,
        members
      }).save();
    });
    const events = await Promise.all(eventPromises);

    // Insert group
    const groupPromises = seedData.groups.map(group => {
      const administrators = group.administrators.map(email => emailToUserId[email]);
      const members = group.members.map(email => emailToUserId[email]);
      return new Group({
        ...group,
        administrators, 
        members
      }).save();
    });
    const groups = await Promise.all(groupPromises);

    //Map event name to event id
    const nameToEventId = {};
    events.forEach(event => {
      nameToEventId[event.name] = event.id;
    });

    //Insert thread
    const threadPromises = seedData.threads.map(thread => {
      const event = nameToEventId[thread.event];
      const members = thread.members.map(email => emailToUserId[email]);
      const creator = emailToUserId[thread.creator];
      return new Thread({
        title: thread.title,
        creator, 
        members,
        event,
      }).save();
    });
    const threads = await Promise.all(threadPromises);

    //Map thread name to thead id
    const titleToThreadId = {};
    threads.forEach(thread => {
      titleToThreadId[thread.title] = thread.id;
    })

    const messagePromises = seedData.messages.map(message => {
      const creator = emailToUserId[message.creator];
      const thread = titleToThreadId[message.thread];
      return new Message({
        creator,
        content: message.content,
        thread,
        dateCreate: message.dateCreate,
        comment: message.comment,
      }).save();
    });
    await Promise.all(messagePromises);



    console.log('Database seeded successfully.');
    mongoose.disconnect();
  } catch (error) {
    console.error('Error seeding database:', error);
    mongoose.disconnect();
  }
}

seedDatabase();
