const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    content: {
        type: String,
        required: true,
    },
    dateCreate: {
        type: Date, 
        default: Date.now,
    }
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;