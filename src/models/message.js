const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    content: {
        type: String,
        required: true,
    },
    thread: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Thread',
        required: true,
    },
    dateCreate: {
        type: Date, 
        default: Date.now,
    },
    comment: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
        required: false,
    }]
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;