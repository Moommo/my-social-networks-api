const mongoose = require('mongoose');

const threadSchema = new mongoose.Schema({
    title : {
        type: String,
        required: true,
        unique: true,
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    members: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    group: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Group',
        required: false,
    },
    event: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event',
        required: false,
    }
});

// Check if add a group and an event
threadSchema.pre('save', function(next) {
    if (!this.group && !this.event){
        next(new Error('You need to use a group or an event'));
    }
    else if (this.group && this.event) {
        next(new Error('You need to use a group or an event not a group and an event'));
    }
    else {
        next();
    }
})


const Thread = mongoose.model('Thread', threadSchema);

module.exports = Thread;