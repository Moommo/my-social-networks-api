const express = require('express');
const Message = require('../models/message');

const router = express.Router();


router.post('/', async (req, res) => {
    const { creator, content, thread, dateCreate, comment } = req.body;

    try {
        const creatorId = await User.find({ email: creator}).distinct('_id');

        const message = new Message({creator:creatorId, content, thread, dateCreate, comment})
        await message.save();
        res.status(201).send(message);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.get('/', async (req, res) => {
    try {
      const messages = await Message.find({}).populate('creator').populate('thread');
      res.status(200).send(messages);
    } catch (error) {
      res.status(500).send(error);
    }
});

router.get('/:threadId', async (req, res) => {
    const { threadId } = req.params;
    try {
      const messages = await Message.findById(threadId).populate('creator').populate('thread');
      res.status(200).send(messages);
    } catch (error) {
      res.status(500).send(error);
    }
});


module.exports = router;