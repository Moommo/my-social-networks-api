const express = require('express');
const Group = require('../models/group');
const User = require('../models/user');

const router = express.Router();

// Create a new group
router.post('/', async (req, res) => {
    const { name, administrators, members } = req.body;
  
    try {
  
      //Recover list of id user
      const administratorsIds = await User.find({ email: { $in: administrators } }).distinct('_id');
      const membersIds = await User.find({ email: { $in: members } }).distinct('_id');
  
      const group = new Group({ name, administrators: administratorsIds, members: membersIds });    
      await group.save();
      res.status(201).send(group);
    } catch (error) {
      res.status(400).send(error);
    }
  });
  
  // Display all group
  router.get('/', async (req, res) => {
      try {
        const groups = await Group.find().populate('administrators').populate('members');
        res.status(200).send(groups);
      } catch (error) {
        res.status(500).send(error);
      }
  });
  
  
  module.exports = router;
