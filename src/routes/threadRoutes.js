const express = require('express');
const Thread = require('../models/thread');


const router = express.Router();

router.post('/', async (req, res) => {
    const { title, creator, members, group, event } = req.body;

    try {
        const thread = new Thread({title, creator, members, group, event});
        await thread.save();
        res.status(201).send(thread);
    } catch (error) {
        res.status(400).send(error);
    }
    
})


router.get('/', async (req, res) => {
    try {
        const threads = await Thread.find().populate('creator').populate('members').populate('group').populate('event');
        res.status(200).send(threads);
    } catch (error) {
        res.status(500).send(error);
    }
})

router.get('/:id', async (req, res) => {
    try {
        const threads = await Thread.findById(req.params).populate('creator').populate('members').populate('group').populate('event');
        res.status(200).send(threads);
    } catch (error) {
        res.status(500).send(error);
    }
})

module.exports = router;