const express = require('express');
const Event = require('../models/event');
const User = require('../models/user');


const router = express.Router();

// Create a new event
router.post('/', async (req, res) => {
  const { name, description, startDate, endDate, location, picture, isPublic, organizers, members } = req.body;

  try {

    //Recover list of id user
    const organizersIds = await User.find({ email: { $in: organizers } }).distinct('_id');
    const membersIds = await User.find({ email: { $in: members } }).distinct('_id');

    const event = new Event({ name, description, startDate, endDate, location, picture, isPublic, organizers: organizersIds, members: membersIds });    
    await event.save();
    res.status(201).send(event);
  } catch (error) {
    res.status(400).send(error);
  }
});

// Display all events
router.get('/', async (req, res) => {
    try {
      const events = await Event.find().populate('organizers').populate('members');
      res.status(200).send(events);
    } catch (error) {
      res.status(500).send(error);
    }
});

router.get('/:id', async (req, res) => {
    const { eventId } = req.params;
    try {
      const event = await Event.findbyId(eventId).populate('organizers').populate('members');
      res.status(200).send(event);
    } catch (error) {
      res.status(500).send(error);
    }
});


module.exports = router;
